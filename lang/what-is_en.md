#### What

Socialissues.tech aggregates issues from Social and Solidarity Economy (SSE) projects. The tool searches for issues in Git-based version control systems (so far Gitlab and Github) label with `socialissues.tech`.

#### Why

We take advantage of the powerful communities that exist around Free Software (FLOSS) to enrich the SSE ecosystem by creating a bridge between the tech and social worlds, making these projects gain importance and visibility thus facilitating entry to new contributors.

- To give visibility to projects outside the SSE.
- For projects to move forward and have more contributors.
- To promote synergies between projects.
- To highlight the importance of welcoming FLOSS projects.

##### What it can do for me?

- As a contributor
   - You will find issues with a direct impact on society.
   - You can bring your tech know-how and provide solutions to SSE entities.
- As a project
   - A space where you can show your project to people who, by other means, would not get to know the initiative.
   - Solutions to your issues from a point of view outside the SSE.
   - Opportunities to document your projects and open the code to new contributors.
   - Get more stuff done with external contributions.

#### Who

Some SSE projects have allied to help each other evolve and improve our ecosystem. They have now shown interest in leading this initiative collectives such as [Coopdevs](https://coopdevs.org/), [Adabits](https://www.adabits.org/), [Dabne](https://www.dabne.net/), [Col·lectivaT](https://collectivat.cat/) and [LliureTic](https://www.lliuretic.cat/).

#### Glossary

- _** Issue **: task or error to be resolved in a project._
- _** FLOSS **: Free/Libre and Open Source Software.
- _** ** Git **: free software version-control system for tracking changes in source code during software development.
- _** SSE **: Social and solidarity economy: way of producing, distributing and consuming people's service, based on cooperation and the common good._
